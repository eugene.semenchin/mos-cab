// Настройка табов //

const tabs = (tabsSelector, tabsHeadSelector, tabsBodySelector, tabsCaptionSelector, tabsCaptionActiveClass, tabsContentActiveClass) => { 
  
  const tabs = document.querySelector(tabsSelector);
  const head = tabs.querySelector(tabsHeadSelector); 
  const body = tabs.querySelector(tabsBodySelector);

  const getActiveTabName = () => { 
    return head.querySelector(`.${tabsCaptionActiveClass}`).dataset.tab 
  }

  const setActiveContent = () => { 
    if (body.querySelector(`.${tabsContentActiveClass}`)) { 
      body.querySelector(`.${tabsContentActiveClass}`).classList.remove(tabsContentActiveClass) 
    }
    body.querySelector(`[data-tab=${getActiveTabName()}]`).classList.add(tabsContentActiveClass) 
  }

  
  if (!head.querySelector(`.${tabsCaptionActiveClass}`)) { 
    head.querySelector(tabsCaptionSelector).classList.add(tabsCaptionActiveClass) 
  }

  setActiveContent(getActiveTabName()) 
  head.addEventListener('click', e => { 
    const caption = e.target.closest(tabsCaptionSelector) 
    if (!caption) return 
    if (caption.classList.contains(tabsCaptionActiveClass)) return 
    if (head.querySelector(`.${tabsCaptionActiveClass}`)) { 
      head.querySelector(`.${tabsCaptionActiveClass}`).classList.remove(tabsCaptionActiveClass) 
    }

    caption.classList.add(tabsCaptionActiveClass) 

    setActiveContent(getActiveTabName()) 
  })
}

tabs('.section__tabs', '.tabs__head', '.tabs__body', '.tabs__caption', 'tabs__caption_active', 'tabs__content_active') 

// tabs('.about__tabs', '.tabs__head', '.tabs__body', '.tabs__caption', 'tabs__caption_active', 'tabs__content_active') 


// Настройка аккордиона //

var acc = document.getElementsByClassName("accordion__item");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}

// Валидация формы //

// Телефон //

window.addEventListener("DOMContentLoaded", function() {
  [].forEach.call( document.querySelectorAll('.tel'), function(input) {
  var keyCode;
  function mask(event) {
      event.keyCode && (keyCode = event.keyCode);
      var pos = this.selectionStart;
      if (pos < 3) event.preventDefault();
      var matrix = "+7 (___) ___ ____",
          i = 0,
          def = matrix.replace(/\D/g, ""),
          val = this.value.replace(/\D/g, ""),
          new_value = matrix.replace(/[_\d]/g, function(a) {
              return i < val.length ? val.charAt(i++) || def.charAt(i) : a
          });
      i = new_value.indexOf("_");
      if (i != -1) {
          i < 5 && (i = 3);
          new_value = new_value.slice(0, i)
      }
      var reg = matrix.substr(0, this.value.length).replace(/_+/g,
          function(a) {
              return "\\d{1," + a.length + "}"
          }).replace(/[+()]/g, "\\$&");
      reg = new RegExp("^" + reg + "$");
      if (!reg.test(this.value) || this.value.length < 5 || keyCode > 47 && keyCode < 58) this.value = new_value;
      if (event.type == "blur" && this.value.length < 5)  this.value = ""
  }

  input.addEventListener("input", mask, false);
  input.addEventListener("focus", mask, false);
  input.addEventListener("blur", mask, false);
  input.addEventListener("keydown", mask, false)

});

});

// Отправка //

const form = document.forms["form"];
const formArr = Array.from(form);
const validFormArr = [];
const button = form.elements["button"];

formArr.forEach((el) => {
  if (el.hasAttribute("data-reg")) {
    el.setAttribute("is-valid", "0");
    validFormArr.push(el);
  }
});

form.addEventListener("input", inputHandler);
button.addEventListener("click", buttonHandler);

function inputHandler({ target }) {
  if (target.hasAttribute("data-reg")) {
    inputCheck(target);
  }
}

function inputCheck(el) {
  const inputValue = el.value;
  const inputReg = el.getAttribute("data-reg");
  const reg = new RegExp(inputReg);
  if (reg.test(inputValue)) {
    el.setAttribute("is-valid", "1");
    el.style.backgroundColor = "#bdf57d";

  } else {
    el.setAttribute("is-valid", "0");
    el.style.backgroundColor = "#f57d7d";
  }
}

function buttonHandler(e) {
  const allValid = [];
  validFormArr.forEach((el) => {
    allValid.push(el.getAttribute("is-valid"));
    console.log(allValid)
    
  });
  const isAllValid = allValid.reduce((acc, current) => {
    return acc && current;
  });

  if (!Boolean(Number(isAllValid))) {
    e.preventDefault();

  }
}

// Анимация //

new WOW().init();